from rest_framework import viewsets
from backend.apps.backend.models import DataModel
from backend.apps.backend.serializers import DataModelSerializer


class DataModelViewSet(viewsets.ModelViewSet):
    queryset = DataModel.objects.all().order_by('-created_at')
    serializer_class = DataModelSerializer
