from rest_framework import serializers
from backend.apps.backend.models import DataModel


class DataModelSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = DataModel
        fields = ('__all__',)
